<h1>openDesk Erprobungskonzept</h1>

**Schnellnavigation Erprobungskonzept**

* [Einleitung](#einleitung)
  * [Überblick](#überblick)
  * [Gegenstand der Erprobung](#gegenstand-der-erprobung)
  * [Bezugsrahmen](#bezugsrahmen)
  * [Zielsetzung](#zielsetzung)
* [Mögliche Erprobungspartner](#mögliche-erprobungspartner)
  * [Allgemeine Kriterien](#allgemeine-kriterien)
  * [Spezielle Kriterien](#spezielle-kriterien)
    * [Personelle Anforderungen](#personelle-anforderungen)
    * [Kapazitätsanforderungen](#kapazitätsanforderungen)
  * [Auswahl](#auswahl)
* [Erprobungsprozess](#erprobungsprozess)
  * [Phasen der Erprobung](#phasen-der-erprobung)
    * [Planung](#planung)
    * [Durchführung](#durchführung)
    * [Überprüfung](#überprüfung)
  * [Bewertung der Erprobung](#bewertung-der-erprobung)
  * [RACI-Matrix](#raci-matrix)
* [Allgemeine Voraussetzungen](#allgemeine-voraussetzungen)
  * [Zeitplanung](#zeitplanung)
    * [Aufgaben mit Bezug auf Timeline](#aufgaben-mit-bezug-auf-timeline)
    * [Meilensteinplanung](#meilensteinplanung)
  * [Betriebsplanung](#betriebsplanung)
* [Produkt](#produkt)
  * [Leistungsbeschreibung](#leistungsbeschreibung)
  * [Funktionale Integration](#funktionale-integration)
* [Infrastruktur](#infrastruktur)
  * [Anforderung Modulkomponenten](#anforderung-modulkomponenten)
  * [Staging](#staging)
  * [Bereitstellung](#bereitstellung)
    * [Open CoDE](#open-code)
* [Monitoring-Prozesse](#monitoring-prozesse)
  * [Logging und Protokollierung](#logging-und-protokollierung)
    * [Ziele der Protokollierung](#ziele-der-protokollierung)
    * [Zweckbindung der Erhebung](#zweckbindung-der-erhebung)
    * [Auswertung der Protokolldateien](#auswertung-der-protokolldateien)
    * [Alarmierung](#alarmierung)
* [Operative Prozesse](#operative-prozesse)
  * [Prozesslandkarte](#prozesslandkarte)
  * [Nutzerperspektive](#nutzerperspektive)
  * [Tool-gestützte Prozessumsetzung](#tool-gestützte-prozessumsetzung)
    * [Incident-Management](#incident-management)
    * [Patch-Management](#patch-management)
  * [Compliance](#compliance)
  * [Sichere Quellen für Open-Source Software (OSS)](#sichere-quellen-für-open-source-software-oss)
* [Management-Prozesse](#management-prozesse)
  * [KPI](#kpi)
    * [IT-Monitoring](#it-monitoring)
    * [Erprobungsbetrieb](#erprobungsbetrieb)
  * [Reporting](#reporting)
  * [Überprüfung/Audit](#überprüfungaudit)
  * [Übergang in den Betrieb](#übergang-in-den-betrieb)
* [Zusammenfassung](#zusammenfassung)
  * [Checkliste Erprobung](#checkliste-erprobung)
    * [Anforderungen an den IT-Betreiber vor Durchführung des PoC](#anforderungen-an-den-it-betreiber-vor-durchführung-des-poc)
    * [Anforderungen an den Aufbau des Erprobungsbetriebs](#anforderungen-an-den-aufbau-des-erprobungsbetriebs)
    * [Anforderungen während der Erprobung](#anforderungen-während-der-erprobung)
    * [Anforderungen zur Evaluierung des PoC](#anforderungen-zur-evaluierung-des-poc)
* [Fußnotenverzeichnis](#fußnotenverzeichnis)

# Einleitung

## Überblick

Mit dem vorliegenden Dokument sollen notwendige technische, infrastrukturelle und organisatorische Anforderungen an die Erprobung des openDesk – Der Souveräne Arbeitsplatz (folgend openDesk) beschrieben werden.

Das Dokument soll dem Leser einen Überblick über die notwendigen Voraussetzungen geben. Die technisch-infrastrukturelle Beschreibung, die operative prozessuale Umsetzung sowie vertragliche Voraussetzungen werden generell beschrieben.
Diesem Ansatz folgend, gibt diese Anforderungsspezifikation einen Überblick über die in folgender Abbildung dargestellten Inhalte.

<div align="center">
<section>
<img src="./media/Beispiel_Anforderungsspezifikation.png">
</section>
<em>Beispiel der Anforderungsspezifikation</em>
<br>
</div>

## Gegenstand der Erprobung

Mit dem Produkt openDesk stellt der IT-Betreiber einen cloudbasierten Web-Arbeitsplatz für den öffentlichen Sektor (Verwaltung, Schulen, Universitäten, Kultur, …) als IT-Service bereit.
Dazu zählen u. a. folgende Bereiche und Funktionen:
-   Produktivität: Textverarbeitung, Tabellenkalkulation, Erstellung von Präsentationen
-	Kollaboration: Gemeinsames Bearbeiten von geteilten Dateien
-	Kommunikation: Durchführung von Video- und Audiokonferenzen, Nutzung von E-Mail und Kurznachrichtendiensten sowie Terminorganisation über elektronische Kalender
-	Übergreifend: Anbindung ausgewählter Fachverfahren (z. B. E-Akte)

<div align="center">
<section>
<img src="./media/Zielbild_openDesk.png">
</section>
<em>Zielbild des openDesk</em><br>
<br>
</div>

Zur Wahrung der digitalen Souveränität werden dabei Alternativen zu den marktbeherrschenden Produkten evaluiert und eingesetzt. Die technische, infrastrukturelle Entwicklung und Bereitstellung soll dabei betreiberunabhängig möglich sein.
Diese visuell dargestellte Produktvision muss für die Betrachtung einer Anforderungsspezifikation in wesentliche Punkte zunächst untergliedert werden, um die Herausforderungen und Spezifikationen zu verstehen und nachvollziehbar erläutern zu können.
1.	Anforderungen an die Funktionalität des Produktes
2.	Anforderungen an die Entwicklung der Produktfunktionalitäten und Produktintegrationen
3.	Anforderungen an die Inbetriebnahme und den operativen Betrieb
4.	Anforderungen an die Lizenz- und Complianceprüfung
5.	Anforderungen an das Vertrags- und Herstellermanagement

## Bezugsrahmen

Das Dokument ist nicht mit einer „Betriebsdokumentation“ gleichzusetzen. Das Erprobungskonzept erweitert die Betriebsdokumentation um Aspekte der Planung und Evaluierung der Erprobung in Kooperation mit einem Partner.
Die Planung geht dabei auf Fragestellungen der zeitlichen Einordnung der einzelnen Prozessschritte im Rahmen der Erprobung ein, während die Evaluierung Informationen liefert, den Erfolg der Durchführung mit einem Erprobungspartner zu bewerten.
Die technisch-betrieblichen Inhalte, die im Detail in der Betriebsdokumentation erläutert sind, werden im vorliegenden Dokument bei Bedarf angeführt. Für eine weiterführende Erläuterung wird auf die entsprechenden Kapitel der Betriebsdokumentation verwiesen.

## Zielsetzung

Dieses Dokument soll IT-Dienstleister des öffentlichen Dienstes befähigen, nach Prüfung der hier aufgeführten Voraussetzungen zu entscheiden, ob und wieweit sie als IT-Betreiber des openDesk fungieren können.

Die notwendigen Aspekte, die Erprobung zu realisieren, werden in den folgenden Kapiteln näher ausgeführt. Die Punkte beinhalten Anforderungen, die an den Erprobungspartner und deren Prozessdarstellung gestellt sind, gefolgt von Anforderungen, die hinsichtlich des Betriebs gelten und wie die Anforderungen und deren Umsetzung protokolliert bzw. evaluiert werden.

Daraus ergibt sich, dass ein Erprobungspartner einem IT-Betreiber gleichgestellt ist. Da sich die Erprobung über einen definierten Zeitraum erstreckt und erst mit positiver Evaluierung am Ende eine Überführung in den produktiven Betrieb stattfinden kann, wird in diesem Dokument bewusst zwischen Erprobungspartner und IT-Betreiber unterschieden.

Die aufgeführte Zielsetzung wird im Verlauf der Erprobung fortlaufend in verschiedenen Formen validiert und abschließend zu einem Gesamtergebnis zusammengeführt.

# Mögliche Erprobungspartner

In diesem Abschnitt werden Empfehlungen ausgesprochen, nach welchen Kriterien die Auswahl eines Erprobungspartners erfolgen und wie ein Auswahlprozess gestaltet sein kann.

## Allgemeine Kriterien

Erprobungspartner sollten anhand folgender Kriterien ausgewählt werden:
-	Die Organisation ist ein IT-Dienstleister des öffentlichen Dienstes und entweder eine juristische Person des öffentlichen Rechts oder eine juristische Person privaten Rechts in vollständiger Trägerschaft der öffentlichen Hand.
-	Die Organisation ist sowohl im Rechenzentrums- als auch im Endgerätebetrieb tätig.
-	Die Organisation verfügt über eigene Rechenzentrumskapazitäten oder hat die Möglichkeit, die Infrastruktur in Rechenzentren zu gestalten.
-	Grundschutzfähigkeit oder vergleichbare Voraussetzungen (z. B. ISO27001[^1] , C5[^2] ) und formalisierte Betriebsprozesse (z. B. ITSM) werden angeboten.
-	Während der Erprobung kann die Organisation auf mindestens 10 bis 15 Anwender pro Kunden, die an der Erprobung teilnehmen möchten, zurückgreifen
-	Die in den folgenden Abschnitten beschriebenen Rahmenbedingungen können erfüllt und eventuell fehlende Voraussetzungen geschaffen werden.

## Spezielle Kriterien

### Personelle Anforderungen

Ein Erprobungspartner (siehe auch Definition Erprobungspartner/IT-Betreiber in [Zielsetzung](#zielsetzung)) muss gewisse Mindestkriterien erfüllen, um als Partner in Frage zu kommen. Diese Kriterien verfolgen die Gewährleistung, dass die Erprobung durch qualifiziertes Personal betreut und gesteuert wird. Die angegebenen Mindeststunden pro Woche sollten in ausreichender Personalstärke (ein Hauptverantwortlicher mit jeweils einem Vertreter) vor, während und nach der Erprobung gegeben sein:
-	56 Stunden pro Woche in der Technik
-	16 Stunden pro Woche Projektmanagement
-	24 Stunden pro Woche Administration / DevOps
-	8 Stunden pro Woche Teamleitende / Ansprechpartner Management
Die hier angegebenen Stunden dienen als Mindestwerte. Sie sind je nach Umfang der Erprobung und des damit verbundenen Erprobungspartners entsprechend anzupassen und für die Auswahl des Erprobungspartners anzuwenden.
Die dargestellten Mitarbeiter können Aufgaben auch in Personalunion erfüllen. Es ist nicht erforderlich, dass es für jede der dargestellten Aufgaben eine einzelne Person mit entsprechendem Stundenumfang geben muss.

### Kapazitätsanforderungen

Neben den personellen Anforderungen, die an den Erprobungspartner gestellt werden, müssen zusätzlich Mindestanforderungen hinsichtlich der Kapazitäten erfüllt werden können. Diese Mindestanforderungen verfolgen das Ziel, einen reibungslosen Betrieb von openDesk im Rahmen der Erprobung zu gewährleisten. Die Minimalanforderungen, die an openDesk gestellt werden, sind in den [Requirements](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/blob/main/docs/requirements.md) auf Open CoDE veröffentlicht.

## Auswahl

Zur Auswahl eines Erprobungspartners sollten die in Kapitel [Allgemeine Kriterien](#allgemeine-kriterien) angegebenen Punkte prozentual bewertet werden. Die Bewertung sollte berücksichtigen, ob es sich um Muss- oder Soll-Kriterien handelt.
Muss-Kriterien im Sinne des vorliegenden Dokuments sind Kriterien, die innerhalb der Organisation bzw. des möglichen Erprobungspartners erfüllt sein müssen. Eine Nichterfüllung ist einer negativen Entscheidung dem Erprobungspartner gegenüber gleichzusetzen.
Soll-Kriterien im Sinne des vorliegenden Dokuments sind Kriterien, die innerhalb der Organisation bzw. des möglichen Erprobungspartners noch nicht erfüllt, aber in einem definierten Zeitrahmen, spätestens jedoch zur Erprobung erfüllt sein können. Eine Nichterfüllung mit Beginn der Erprobung ist ggf. durch zeitlich begrenzte Nachbesserung zu korrigieren. Sollte auch die Nachbesserung bzw. die Erfüllung der Kriterien nach zugesagter Nachbesserungsfrist nicht eingehalten werden, ist dies mit einer negativen Gesamtentscheidung der Erprobung gleichzusetzen.

# Erprobungsprozess

## Phasen der Erprobung

Es wird empfohlen, die Erprobung des openDesk in verschiedene Teilabschnitte zu untergliedern, die den jeweiligen Fortschritt wiedergeben. Die einzelnen Phasen werden in den folgenden Unterkapiteln kurz dargestellt.

### Planung

Die Planung umfasst u. a. folgende Punkte:
-	Anforderungen an openDesk
-	Anforderungen zur [Betreiberauswahl](#mögliche-erprobungspartner)
-	Chronologische Planung der Erprobung
-	Anforderungen an die Evaluierung
-	Festlegung von Rahmenbedingungen (z. B. Kommunikationswege, Ansprechpartner, Testuser)

### Durchführung

Die Durchführung umfasst u. a. folgende Punkte:
-	Einführen und Etablieren definierter Kommunikationswege
-	Durchführung des Proof of Concepts (PoC)
-	Tracking des Fortschritts
-	Fortlaufender Review und ggf. anpassen abgestimmter Prozesse
-	Abschluss des PoC
-	Tracking

### Überprüfung

Die [Überprüfung](#bewertung-der-erprobung) umfasst u. a. folgende Punkte:
-	Korrektheit der Liefergegenstände
-	Korrektheit der Angaben
-	Tracking von möglichen Mängeln
-	Tracking von möglichen Änderungswünschen
-	Audit[^3]

Wird bei der Überprüfung des Erprobungsbetriebs der Erfolg festgestellt und der Erprobungspartner ist als IT-Betreiber des openDesk qualifiziert, erfolgt ggf. ein Übergang in den Produktivbetrieb. Dieser wird näher in Kapitel [Übergang in den Betrieb](#übergang-in-den-betrieb) erläutert.

## Bewertung der Erprobung

Die folgende Empfehlung zur Bewertung eines Erprobungsprozesses beruht auf der Umsetzung der in den weiteren Kapiteln beschriebenen Schritte und Beispiele.
Vor der Bewertung werden folgende Fragen beantwortet:
-	Welche Umsetzungsziele bzw. Anforderungen werden als MUSS, SOLL oder KANN bewertet?
-	Ab welchem Prozentsatz gilt eine Anforderung als erfüllt?
-	In welchem Verhältnis werden die MUSS, SOLL, KANN-Kriterien gegeneinander gewertet? Hier wird eine Punktewertung empfohlen. <br> Beispiel: MUSS = 100, SOLL = 50, KANN = 10.
-	Ab welchem durchschnittlichen Erfüllungsgrad in Prozent wird die Erprobung insgesamt als Erfolg gewertet? Beispiel: 80%
-	Welche Anforderungen müssen für den Gesamterfolg zwingend erfüllt werden (MUSS, SOLL, KANN)? <br> Beispiel: Nur MUSS-Anforderungen sind zwingend.
Im ersten Bewertungsschritt wird eine tabellarische Auflistung der Umsetzungserfolge erzeugt, die auch den Erfolg der einzelnen Schritte ausweist.

Beispiel:

| Nr. | Bewertungskriterium                               | Anforderungsart <br> (Betreiberdefinition) | Umsetzungserfolg |
| --- | ------------------------------------------------- | ------------------------------------------ | ---------------- |
| 1   | Erfüllbarkeit allgemeine Voraussetzungen          | MUSS                                       | 90% (Erfolg)     |
| 2   | Vollständige Einsetzbarkeit aller Produktfeatures | SOLL                                       | 80% (Erfolg)     |
| 3   | Erfolgreiche Grundschutzprüfung                   | MUSS                                       | 95% (Erfolg)     |
| 4   | Umsetzbarkeit der Infrastrukturanforderungen      | MUSS                                       | 95% (Erfolg)     |
| 5   | Einbindung in Logging/Monitoring                  | SOLL                                       | 80% (Erfolg)     |
| 6   | Einbindung in operative Prozesse                  | SOLL                                       | 50% (Fehlschlag) |
| 7   | Herstellung von Compliance                        | MUSS                                       | 95% (Erfolg)     |
| 8   | Einführung KPI Monitoring                         | KANN                                       | 10% (Fehlschlag) |
| 9   | Durchführung/Einführung Reporting                 | KANN                                       | 20% (Fehlschlag) |

*Beispiel für Bewertung des Umsetzungserfolgs*


Im zweiten Schritt werden zwingende Umsetzungserfolge kontrolliert und der jeweilige Prozentsatz im Verhältnis zur Gesamtbewertung hinzugefügt.

| Nr. | Bewertungskriterium                               | Umsetzungserfolg | Erfolgswert in Punkten |
| --- | ------------------------------------------------- | ---------------- | ---------------------- |
| 1   | Erfüllbarkeit allgemeine Voraussetzungen          | 90% (Erfolg)     | 90                     |
| 2   | Vollständige Einsetzbarkeit aller Produktfeatures | 80% (Erfolg)     | 40                     |
| 3   | Erfolgreiche Grundschutzprüfung                   | 95% (Erfolg)     | 95                     |
| 4   | Umsetzbarkeit der Infrastrukturanforderungen      | 95% (Erfolg)     | 95                     |
| 5   | Einbindung in Logging/Monitoring                  | 80% (Erfolg)     | 40                     |
| 6   | Einbindung in operative Prozesse                  | 50% (Fehlschlag) | 25                     |
| 7   | Herstellung von Compliance                        | 95% (Erfolg)     | 95                     |
| 8   | Einführung KPI Monitoring                         | 10% (Fehlschlag) | 1                      |
| 9   | Durchführung/Einführung Reporting                 | 20% (Fehlschlag) | 2                      |

*Beispiel für die Erreichung des Umsetzungserfolgs*

**Beispielauswertung**

- Zwingende Anforderungen erfüllt: ja
- Mögliche Gesamtpunkte: 570
- Erreichte Punkte: 483
- Prozentwert: 84% (Erfolg)

## RACI-Matrix

In der nachfolgenden RACI-Matrix wird dargestellt, welche Hauptthemenkomplexe im Rahmen der Erprobung auftreten bzw. zu erwarten sind.

Diese Themen sind mit entsprechenden Verantwortlichkeiten gem. RACI-Matrix-Klassifizierung[^4] den einzelnen Beteiligten zugeordnet.

<table>
    <tr>
        <th></th>
        <th colspan="4"> Organisation</th>
    </tr>
    <tr>
        <th> Thema</th>
        <td>Produktverantwortung openDesk</td>
        <td>IT-Betreiber</td>
        <td>Kunde / User</td>
        <td>Hersteller Einzelanwendungen</td>
    </tr>
    <tr>
        <td>Test und Freigabe Produkt openDesk</td>
        <td>R/A</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Test und Freigabe für Betrieb</td>
        <td>C</td>
        <td>R/A</td>
        <td>I</I></td>
        <td></td>
    </tr>
    <tr>
        <td>Infrastrukturbetrieb</td>
        <td></td>
        <td>R/A </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Anwenderunterstützung</td>
        <td></td>
        <td>R/A</td>
        <td>I </td>
        <td>C </td>
    </tr>
    <tr>
        <td>Produktsupport Einzelanwendungen</td>
        <td></td>
        <td>I </td>
        <td></td>
        <td>R/A </td>
    </tr>
    <tr>
        <td>Grundschutzprüfung</td>
        <td></td>
        <td>R </td>
        <td>A </td>
        <td>C/I </td>
    </tr>
    <tr>
        <td>Datenschutz: Verantwortung</td>
        <td></td>
        <td>C</td>
        <td>A</td>
        <td></td>
    </tr>
    <tr>
        <td>Datenschutz: technisch-organisatorische Maßnahmen</td>
        <td>C </td>
        <td>R/A </td>
        <td></td>
        <td></td>
    </tr>
</table>

*Beispiel für die Darstellung einer RACI-Matrix zur Erprobung*

# Allgemeine Voraussetzungen

## Zeitplanung

Der IT-Dienstleister gestaltet eine Timeline, über die der Fortschritt der Erprobung verfolgt werden kann. Erfahrungswerte[^5] zeigen, dass die Timeline unter Berücksichtigung aller Faktoren nicht weniger als sechs bis neun Monate, aber auch nicht mehr als zwölf bis sechzehn Monate betragen sollte. Relevant für die Planung der Timeline sind dabei die Phasen [Planung](#planung), [Durchführung](#durchführung) und [Überprüfung](#überprüfung). Auch hier zeigte sich, dass eine Drittelung der Timeline empfehlenswert ist.

### Aufgaben mit Bezug auf Timeline

Die Timeline sollte dabei über die Aufgaben der Erprobung hinausgehen. Diese Aufgaben umfassen vorgelagerte Tätigkeiten, laufende steuernde Aufgaben der Erprobung und die Evaluierung nach Beendung des Erprobungsprozesses.

Managementaufgaben beinhalten u. a.

-	Ressourcenplanung
-	Business Case Definition
-	Personalplanung (Qualifikation, Tagesgeschäftsplanung)

Laufende steuernde Aufgaben beinhalten u. a.
-	Fortschrittstracking Erprobung
-	Erstellung von Zwischenberichten
-	Steuerung der Ressourcen
-	Aufbereitung der KPI

Evaluierung beinhaltet u. a.
-	Erstellung Erfolgsauswertung Erprobungsbetrieb
-	Ggf. Erstellung eines Abschlussberichtes
-	Konzepterstellung für einen Betrieb
-	Ggf. Konzepterstellung für die Überführung der Erprobung in einen Betrieb


### Meilensteinplanung

Für die in Kapitel [Aufgaben mit Bezug zur Timeline](#aufgaben-mit-bezug-auf-timeline) dargestellten Aufgaben im Rahmen der Erprobung wird empfohlen, eine Meilensteinplanung aufzusetzen. Die Meilensteinplanung sollte in Anlehnung an die in Kapitel [Phasen der Erprobung](#phasen-der-erprobung) dargestellten Phasen der Erprobung erfolgen und so eine Zeitplanung der Phasen mit definierten Zwischenzielen verfolgen.
Die definierten Zwischenziele im Rahmen der Meilensteinplanung sollten seitens des Erprobungspartners in Form von Zwischenberichten verfasst und mit dem ZenDiS abgestimmt, dokumentiert werden. Die Form der Dokumentation sollte dabei mindestens die folgenden Kapitel beinhalten:

- Einleitung: <br>
Die Einleitung dient zur Darstellung des zeitlichen Bezugsrahmens und der Zielsetzung des Zwischenberichts sowie einer Einordnung des prozentualen Fortschritts der Erprobung.
-	Meilensteinzusammenfassung: <br>
Die Zusammenfassung umfasst durchgeführte Aufgaben im Rahmen des jeweiligen Meilensteins und die Darstellung ggf. aufgetretener Probleme. Der Schwerpunkt sollte sich auf die Analyse folgender Punkte beziehen:
    - Abweichungen zur Planung
    - Verbrauchte Ressourcen
    - Risikodarstellung
- Auswirkungen auf die Meilensteinplanung: <br>
Die Risikodarstellung sollte näher erläutert werden und es sollten Ableitungen getroffen werden, wie die Meilensteinplanung angepasst werden muss. Der Schwerpunkt sollte sich auf die folgenden Punkte beziehen:
    - Neue Anforderungen aus der Risikodarstellung
    - Definition neuer Akzeptanzkriterien
- Nächster Meilenstein <br>
Die Planung des aktuellen Meilensteins wird dargestellt und die im vorangegangenen Kapitel dargestellten Ableitungen werden aufgenommen. Der Schwerpunkt sollte sich auf die folgenden Punkte beziehen:
    - Abgrenzung zwischen der bestehenden Planung und neuer Aufgaben
    - Definition klarer Verantwortlichkeiten für neue Aufgaben
- Zusammenfassung: <br>
Abschließend folgt eine Zusammenfassung der Erfahrungen und eine kurze Darstellung, welche Maßnahmen eingeleitet wurden, um vergleichbare Probleme im weiteren Verlauf der Erprobung zu vermeiden.

## Betriebsplanung

Für die Betriebsplanung ist im Vorfeld zu definieren, ob die Erprobung in einem dedizierten Rechenzentrum, einer eigenen Cloud des Erprobungspartners oder bei einem beauftragten Cloud-Provider durchgeführt wird.
Je nach Entscheidung sind die Zeitplanungen aus Kapitel [Zeitplanung](#zeitplanung) anzupassen. Die technischen Anforderungen an den Betrieb sind:
-	Verwendung von Kubernetes
-	Deployment mittels Helm-Chart
-	Verwendung von Container-Registry

Sie müssen um nachfolgend aufgelistete Planungen erweitert werden:
-	Gewährleistung der technischen Voraussetzungen gem. Betriebsdokumentation
-	Beauftragung / Abstimmung aller technischen Partner (intern/extern)
-	Klärung der Anforderungen bzgl. BSI IT-Grundschutz und DVS (insbesondere und auch ggü. beauftragten Drittparteien)
-	Planung des Austausch'/der Integration ggf. notwendiger Zusatzkomponenten
-	Aufbau notwendiger Strukturen (personell, Hardware, Software und Prozesse)
-	Finanzielle Mittel

# Produkt

## Leistungsbeschreibung

Für die betriebliche Bereitstellung müssen gem. Betriebsdokumentation mehrere Anforderungen erfüllt sein:
-	Erwartete vertraglich vereinbarte Leistungen an die Module
-	Kriterien für den Prozess
-	Bereitstellung der erforderlichen Ressourcen
-	Prozesssteuerung anhand der vereinbarten Kriterien
-	Bereitstellung der Dokumentation

## Funktionale Integration

Für die Integration der verschiedenen Komponenten / Module von openDesk werden durch die Hersteller die notwendigen Artefakte, d. h. insbesondere Container-Images und Helm-Charts, bereitgestellt.

Daraus lässt sich eine Deploymentautomatisierung[^6] ableiten, die für eine CI-basierte Steuerung der Deployments in verschiedenen Stages des Projektes sorgt.

Bei Bedarf kann die Deploymentautomatisierung durch ein DevSecOps-Team erweitert werden, um die Anforderungen an den (Probe-)Betrieb sowie das Deployment von openDesk zu realisieren.
Der Deploymentprozess beim Erprobungspartner sollte darauf ausgerichtet sein, die oben beschriebenen Verfahren zur Modulintegration bestmöglich mit eigenen Strategien und geeigneten Tools zu unterstützen.

Neben einem bereits vorhandenen Sicherheitskonzept, welches geeignete Tools für Sicherheitsscans beinhaltet, wird empfohlen, HelmDiff zu nutzen, um das einwandfreie Deployment zu verifizieren. Für den Fall eines fehlerhaften Deployments sind entsprechende Backup bzw. Rollback Strategien zu definieren. Für auftretende Fehler, die im Rahmen des CI/CD entstehen sollten ebenfalls Feedbackloops eingerichtet werden, um die Fehler identifizieren zu können und schnellstmöglich Korrekturen herbeizuführen.

# Infrastruktur

## Anforderung Modulkomponenten

Die Anforderungen an die Modulkomponenten werden mehrstufig umgesetzt. Dabei gibt es grundsätzlich zunächst drei bereitstellende Parteien, die hier stichpunktartig mit Ihren Aufgaben gelistet sind.
- Open-Source-Produkthersteller
  - Entwicklung und Bereitstellung der Modulkomponente
  - Container inkl. Doku und Helm-Charts
  - Artefakte auf Open CoDE
- openDesk DevSecOps
  - Integration
  - Implementation
  - Quality Gateway
- IT-Betreiber
  - Rollout
  - Betriebsmanagement
  - Weiterentwicklung

## Staging

Das Produkt openDesk ist als modular erweiterbarer und konfigurierbarer IT-Service zur endgeräte-unabhängigen Nutzung konzipiert. Dabei werden Open-Source-Produkte unterschiedlicher Hersteller eingesetzt, die ganz oder teilweise Informationen und Interaktionen miteinander austauschen.
Der IT-Betreiber des Produktes openDesk muss sich der Herausforderung der Modularität der OS-Produkte, der funktionellen Integrität der Modulkomponenten und der für den Betrieb erforderlicher infrastrukturellen Kompatibilität und Servicebereitstellung stellen. Sowohl in der initialen Bereitstellung, dem herstellerbezogenen Update- und Patchmanagement sowie dem infrastrukturellen Update- und Patchmanagement.

## Bereitstellung

openDesk ist in mehrere Module gegliedert. Bei diesen Modulen handelt es sich dabei nicht nur um die - in seinen Funktionen für die Nutzer erkenn- und erlebbaren Produkte mit ihren Funktionen - , sondern auch um Module die dem Monitoring, dem Logging oder generell dem Infrastruktur- und Applikationsaufbau dienen. Jedes Modul verfügt über ein eigenes Installationshandbuch. Sofern zukünftig die Möglichkeit der Auswahl aus unterschiedlichen Open-Source-Software-Angeboten je Modul besteht, werden auch in den Modulen unterschiedliche Installationsanweisungen bzw. Instruktionen   erstellt und gepflegt.
Die Installation der Software Komponenten erfolgt automatisiert aus der CI/CD Pipeline des IT-Betreibers. Folgende Build- und Deployment-Tools sind empfehlenswert
-	Open CoDE / GitLab
-	Artifactory
-	Ansible Tower

### Open CoDE

Helm-Charts der Hersteller werden auf Open CoDE bereitgestellt. Ggf. erfolgt dies über einen zusätzlich zur Upstream-Veröffentlichung des Herstellers stattfindenden Build auf Open CoDE. Hierbei ist jedoch darauf zu achten, dass die Versionierung des Upstream und auf Open CoDE identisch verlaufen.
Die Hersteller wählen für ihre Helm-Charts eine OSI-kompatible Lizenz, die ebenfalls gemäß den Vorgaben von reuse.software  (vergl. https://reuse.software) annotiert sein sollten.
Helm-Charts die für das Projekt entwickelt werden, sollen auf Open CoDE gebaut und aus den dortigen Registries bezogen werden. Diese sind gemäß den Lizenzvorgaben des Projekts bereitzustellen und mit den entsprechenden Lizenzmetainformationen zu versehen (vergl. https://reuse.software/).

<div align="center">
<section>
<img src="./media/Beispielhafte_Darstellung_Bereitstellungsprozess.png">
</section>
<em>Beispielhafte Darstellung des Bereitstellungsprozesses über Open CoDE</em>
<br>
</div>

# Monitoring-Prozesse

Mit der Erprobung sollten geeignete Mittel eingesetzt werden, die ein Monitoring von openDesk ermöglichen. Das Monitoring sollte dabei primär zielgerichtet auf die Erfolgskriterien der Erprobung ausgerichtet sein. Für einen späteren Betrieb können bereits während der Erprobung Kriterien für ein Monitoring mit eingebunden und verprobt werden.

Mögliche Monitoring Items sind u.a.:

- Verfügbarkeitsüberwachungen der Services / Produktmodule
- Ressourcenauslastung (z. B. CPU, Speicher, RAM, etc.) der Infrastrukturkomponenten je Service / Produktmodul
- Nutzungsszenarien, z. B.
  - Anzahl der laufenden Videokonferenzen
  - Angemeldete, aktiv nutzende Teilnehmer je Services / Produktmodul
  - Aktuelle aktive Zugriffe je Services / Produktmodule
- Sicherheitsrelevante Überwachungen (Schwachstellenscan) durch das SIEM-System
- CERT-Management
- Zertifikats-Management

## Logging und Protokollierung

### Ziele der Protokollierung

- Identifikation von Problemen und ungewollten Betriebszuständen, die gemäß SLA zugesicherte Eigenschaften beeinträchtigen bzw. beeinträchtigen können:
  - Abweichungen zu Betriebsstandards,
  - Betriebsstörungen bzw. Früherkennung von Betriebsstörungen.
- Identifikation von Problemen und ungewollten Betriebszuständen, die die Sicherheit beeinträchtigen bzw. beeinträchtigen können:
  - Abweichungen zu Sicherheitsstandards,
  - Detektion und Aufklärung von Sicherheitsvorfällen,
  - Detektion und Aufklärung von Angriffsversuchen,
  - Erkennung der Umgehung von Sicherheitsvorgaben.
- Einhaltung datenschutzrechtlicher Vorgaben und Vorgaben des IT-Grundschutzes gemäß BSI:
  - Nachweisbarkeit des regelkonformen, ordnungsgemäßen Betriebs,
  - Nachvollziehbarkeit veranlassungsbezogener administrativer Tätigkeit,
  - Durchführung von Protokollauswertungen im Rahmen des IKS (Internes Kontrollsystem).

### Zweckbindung der Erhebung

Die Erhebung von Protokolldaten erfolgt nur zur Erreichung der o. g. Ziele.

### Auswertung der Protokolldateien

Die Auswertung von Protokolldaten erfolgt nur zur Erreichung der o. g. Ziele.

### Alarmierung

Monitoring- und Loggingkomponenten werden effektiv, durch den Einsatz einer zusätzlichen Alarmierungskomponente, ergänzt. Diese Alarmierungskomponente unterstützt das visuelle Monitoring sowie das permanente Logging um direkte Alarmierungen, wenn bestimmte Ereignisse eintreten, die durch erstellte Metrik-Daten ausgelöst werden. Beispiele sind u. a.:

- Ressource nähert sich Kapazitätsgrenze
- Verarbeitungsjobs werden nicht ausgeführt
- Knoten in einem Not Ready-Zustand

# Operative Prozesse

## Prozesslandkarte

Für einen operativen Betrieb des openDesk ist eine optimale Einbettung in die Prozesslandkarte des IT-Betreibers erforderlich. Denn mit der Ausbringung des openDesk als Web-Arbeitsplatz greifen viele unterschiedliche Gewerke und IT-Architekturen ineinander und müssen funktional miteinander agieren.

Nachfolgende Abbildung visualisiert eine vereinfachte Prozesslandkarte mit wesentlichen Kernprozessen zur Erbringung von Leistungen und Services und stellt so die für ein Unternehmen kritischen Geschäftsprozesse dar.

<div align="center">
<section>
<img src="./media/Beispielhafte_Prozesslandkarte.png">
</section>
<em>Beispielhafte Prozesslandkarte</em><br>
<br>
</div>

Die Prozesslandkarte wird in drei Prozesskategorien dargestellt:

1. Strategische Prozesse <br>
Prozesse mit der Aufgabe, die zielorientierte Führung des Unternehmens sicherzustellen
1. Primärprozesse <br>
Prozesse, deren Aktivitäten direkten Bezug zum Produkt/Service haben (direkter Beitrag zur Wertschöpfung)
1. Unternehmensprozesse <br>
Prozesse, die notwendig sind, um das Service-Spektrum des Unternehmens zu ermöglichen/auszuführen.

In den nachfolgenden Abschnitten werden die Anforderungen hinsichtlich der Primärprozesse, also mit direktem Produkt/Service Bezug, stichpunktartig beschrieben. Die Beschreibung erfolgt aus drei Perspektiven:

- Nutzerperspektive
- Infrastrukturperspektive (Systemmanagement, d. h. System Monitoring, -alarmierung, Zertifikate, Patching)
- Software-Überwachung (Compliance, CERT)

Die operative Umsetzung und Behandlung dieser Betriebsprozesse erfolgt optimal in einem ITSM-System, um Kunden- und Nutzerinformationen mit Produkt- und Systeminformationen zu verknüpfen und somit be- und auswertbar machen zu können.

## Nutzerperspektive

Die Nutzerperspektive ist bestimmt von den subjektiven Eindrücken bzw. dem Empfinden des Nutzers im Umgang mit openDesk. Vor allem ist sie gekennzeichnet durch:

- Bewertung des funktionalen Umfangs
- Reaktionszeiten beim Starten von Funktionen
- Interaktion der Funktionen untereinander
- Bewertung des Umgangs im täglichen Betrieb

Dieses Empfinden kann durch nachfolgende Faktoren beeinflusst sein

- Funktionales Fehlverhalten durch Bedienfehler
- Übertragungswege
- Eingesetzte Geräte beim Anwender
- Eingesetzte Software (insbesondere Browser)

Um den Erfolg in der Zustimmung durch den Nutzer zu erhöhen, ist eine effektive, effiziente und nutzerorientierte Supportstruktur zu implementieren.

## Tool-gestützte Prozessumsetzung

### Incident-Management

Das Incident-Management dient dem kontrollierten, standardisierten Umgang mit Störungen und Unterbrechungen (Incidents) des IT-Betriebs.

Der Incident-Management-Prozess ist gekennzeichnet durch:

- Gesamtsteuerung des Entstörungsprozesses
- Gezielte Meldung von Betriebsstörungen / Sicherheitsvorfällen
- Schnellstmögliche und sichere Wiederherstellung der Serviceleistung
- Anlehnung an ITIL zur Abdeckung relevanter organisatorischer und technischer Aspekte
- Nutzung geeigneter ITSM-Tools zur Bearbeitung durch dedizierte Supportinstanzen
- Nachvollziehbarkeit des Status bzw. der Lösung

<div align="center">
<section>
<img src="./media/Beispiel_Einbettung_Incident_Management_in_ITIL_Prozesse.png">
</section>
<em>Beispiel zur Einbettung eines Incident-Management-Prozesses in die ITIL-Prozesse (getrennt nach Service Delivery und Service Support)</em><br>
<br>
</div>

Der Incident-Management-Prozess für Meldungen durch Anwender und Überwachung der systemisch gesetzten Schwellen sollte beispielhaft die folgenden Schritte beinhalten:

- Identifizierung
  - Meldung durch Überwachung der Systeme
  - Meldung durch den Anwender über bspw. Service Desk
- Erfassung
  - Vollständige Erfassung über Art und Umfang
  - Versehen mit einem Zeitstempel
  - Bewertung der Auswertung und Dringlichkeit
  - Ableitung der Priorität
- Kategorisierung
  - Unterscheidung nach Hardware, Software oder Bedienung
- Priorisierung
  - Ersteinschätzung durch Service Desk
  - Bestimmt die Geschwindigkeit der Abarbeitung
  - Kann durch Fachbereich angepasst werden
- Untersuchung und Diagnose
  - Erstdiagnose und Versuch der Erstlösung durch Service Desk
  - Bei Nicht-Erfolg erfolgt Weiterleitung an fachliche Ansprechpartner
  - Bei erforderlichen detaillierten technischen Kenntnissen wird ein Third Level Support hinzugezogen
- Lösung und Wiederherstellung
  - Lösung muss getestet werden
  - Dokumentation im ITSM-Tool
  - Kommunikation der Lösung an den Anwender
- Abschluss
  - Qualitätssicherung der Lösung, der Dokumentation und der Kategorisierung
  - Wenn Lösung nur vorübergehend ist, erfolgt Eröffnung einer Problemuntersuchung

### Patch-Management

Um die Abstellung und Vermeidung von Schwachstellen im Rahmen der Erprobung sicherzustellen, ist eine Patch-Management-Prozess zu etablieren bzw. ein beim Erprobungspartner bereits implementierter Patch-Management-Prozess an openDesk anzugleichen.

Das Ziel sollte sein, kritische Patches schnellstmöglich und nicht-kritische Patches innerhalb einer definierten Zeit (z. B. drei Monaten) zu installieren.

Folgende Untergliederung des Patch-Managements findet Berücksichtigung:

- Patch
  - Untergliederung nach Sicherheits- und Funktionspatch
  - Korrigiert Schwachstellen des jeweiligen Bereiches
- Hotfix
  - Schnell verfügbar gemachter Patch
  - Korrigiert kritische Schwachstellen bzw. Ausführungsfehler mit größeren Auswirkungen
  - Keine Anpassung der Softwareversion
- Update
  - Zusammenfassung mehrerer Patches
  - Anpassung der Nebenversion zur Kennzeichnung der Softwarevariante
  - Kleine Funktionsänderungen möglich
- Kombi-Patch
  - Beinhaltet Sicherheits- und Funktionspatch
  - Korrigiert Schwachstellen in beiden Bereichen
  - Analoge Regelungen im Umgang wie mit Patch
- Änderung
  - Geplante Fortentwicklung des Funktions-/Leistungsumfangs
  - Betrifft Software und/oder IT-Infrastruktur
- CERT
  - CERT = Computer Emergency Response Team
  - Öffentliche Organisation für Computersicherheit
  - Verschickt Advisories (Warnmeldungen) zu bekannten sicherheits- und verfügbarkeitsrelevanten Vorfällen in Computer Systemen
  - Enthält Verweise auf Patches oder Workarounds zur Beseitigung der Schwachstelle
- Upgrade
  - Wesentliche Erweiterung der Funktionen einer Software
  - Entspricht einer neuen Hauptversion
  - Wird über den Prozess des Release-Managements behandelt

## Compliance

Für Open-Source gibt es eine Vielzahl von spezifischen Lizenzen, so dass bei Nutzung von Open-Source-Artefakten ggf. eine Vielzahl unterschiedlicher Lizenzen mit den entsprechenden Lizenzbedingungen zu betrachten ist.

Aufgabe der Lizenz-Compliance ist es, fortlaufend eine Prüfung der eingesetzten Open-Source Software (OSS) durchzuführen, um rechtzeitig Probleme in der Lizensierung erkennen zu können.

Hierbei spielen nicht nur die Lizenzbedingungen der originär eingesetzten Artefakte eine Rolle. Ebenso müssen Änderung der eingesetzten Software in den Blick genommen werden, die Verwendungsart der Software (intern, Vertrieb etc.) oder die unterschiedlichen Interessen der Projektpartner oder entsprechender Hersteller mit Blick auf Produkteigenschaften oder Vermarktungsstrategien.

*Hinweis:*

Die Regeln nach denen die Compliance-Prüfung durchgeführt wird und die Bewertung der erkannten Lizenzen für die eingesetzten Artefakte, unterliegen im Detail der Compliance-Prüfung des IT-Betreibers, da diese die Sicherstellung der Verpflichtungen aus den Lizenzbedingungen prüft.

## Sichere Quellen für Open-Source Software (OSS)

*Hinweis:*

Grundsätzlich dürfen im Produkt openDesk nur Artefakte aus sicheren Quellen eingesetzt werden.

In der OSS-Welt gibt es eine unterschiedliche Anzahl von Bezugsquellen für OSS-Artefakte. Dies ist nicht nur bei unterschiedlichen Artefakten der Fall, sondern auch bei einem Artefakt, welches aus verschiedenen Quellen stammen kann. Darüber hinaus gibt es Derivate der Artefakte, die im Kern eine identische Funktionalität bereitstellen und zusätzlich erweitert worden sind.

Nicht alle diese Quellen sind sichere Quellen, da nicht jede Community einen dem Betreiber-Standard genügenden Qualitätssicherungsprozess nutzt. Es ist daher auch bei der Compliance-Prüfung wichtig, die Lieferanten daraufhin zu bewerten, ob diese als sichere Quelle eingeordnet werden können oder nicht. Die Regeln hierfür sind zu definieren und dokumentieren.

<div align="center">
<section>
<img src="./media/Darstellung_sicherer_Quellen_fuer_OSS.png">
</section>
<em>Beispielhafte Darstellung sicherer Quellen für Open-Source Software (OSS)</em>
<br>
</div>

# Management-Prozesse

## KPI

Die Definition von Key Performance Indikatoren (KPI) dient dazu, allgemeinen Überblick über den Erprobungsbetrieb, die darin verwendeten Infrastrukturkomponenten und deren Performance zu erhalten.

Die KPI der Erprobung lassen sich in die beiden Bereiche IT-Monitoring und Erprobungsbetrieb untergliedern. Die Bereiche werden nachfolgend erläutert.

### IT-Monitoring

"IT-Monitoring umfasst ein breites Spektrum an Monitoring-Software, Monitoring Tools und Produkten, mit denen Analysten feststellen, ob die IT-Systeme online sind und das erwartete Service Level erfüllen, während sie zudem gleichzeitig erkannte Probleme beheben."[^7] Eingesetzte Tools zum IT-Monitoring reichen von einfachen Tools zur Überprüfung bis hin zu komplexen Tools, die tiefgreifende Performanceanalysen durchführen und im Fall von vermuteten Problemen automatisierte Korrekturen auslösen können.

Mit zunehmender Komplexität von IT-Umgebungen, hat sich das IT-Monitoring, zu dem auch das Netzwerk- und Anwendungs-Monitoring gehört, weiterentwickelt. Dies beinhaltet unter anderem auch das Monitoring Cloud-basierter Systeme. Die verschiedenen Tools unterscheiden sich deutlich im Ansatz wie auch in der Komplexität. Daher ist individuell zu entscheiden, welches der verfügbaren Tools während der Erprobung zum Einsatz gebracht wird.

Für das Monitoring der relevanten KPI stehen unterschiedliche IT-Tools am Markt zur Verfügung. Bei der Auswahl sollte darauf geachtet werden, dass das verwendete IT-Tool sowohl eine User-freundliche grafische Darstellung der zu überwachenden IT-Daten gewährleisten sowie auch eine Schnittstelle zu einem automatisierten Alarmierungs-Tool ermöglicht.

Empfehlenswerte Tools zum Monitoring:

- Grafana Monitoring
- Thanos
- Alertmanager

Im Erprobungsbetrieb richtet sich das IT-Monitoring primär auf die KPI, die für einen späteren Betrieb eine mögliche Erhebung zulassen. Zu unterscheiden ist zwischen Anforderungen, also KPI, ohne die eine Erprobung nicht evaluiert werden kann, und beispielhaften KPI, die je nach Ausrichtung des Erprobungsbetriebs geführt werden können.

Anforderungen sind:

- Server Downtime (MUSS)
- KPI zur Auslastung
  - CPU (MUSS)
  - RAM (MUSS)
  - Bandbreite der Verbindung (MUSS)
  - Anzahl Nutzer Gesamt (MUSS)
- Mean Time to Patch (MUSS)
- Anzahl Bugs/Incidents/Schwachstellen (MUSS)

Beispielhafte KPI, die einen Mehrwert in der Auswertung bzw. Evaluierung liefern, sind:

- KPI zur Auslastung
  - Anzahl gleichzeitiger Nutzer je Komponente (SOLL)
  - Verbindungsgeschwindigkeit der Komponenten (KANN)

Die angegebenen Kennzahlen sollten in die Kategorien MUSS, SOLL und KANN untergliedert werden, um eine Gewichtung für das Reporting zu ermöglichen. Welche Kriterien mit welcher Gewichtung betrachtet werden, ist in Übereinstimmung vor Beginn des Erprobungsbetriebs zwischen den beiden Partnern zu definieren. **Die angegebene Einstufung ist als Vorschlag zu verstehen.**

### Erprobungsbetrieb

Die KPI für den Erprobungsbetrieb betrachten hingegen die zielgerichtete Evaluierung. Der Erprobungspartner und das ZenDiS verständigen sich auf KPI, anhand derer während und nach dem Erprobungsbetrieb der Erfolg bewertet wird.

Mögliche KPI für den Erprobungsbetrieb sind:

- Prozentsatz Erfüllung der Anforderungen der Checkliste (MUSS)
- Anzahl Fehler der Systemtests (SOLL)
- Anzahl integrierter Komponenten (SOLL)
- Zeit bis zum Deployment neuer Komponenten (SOLL)
- Zeit für die Integrationsanpassung (KANN)
- Anzahl Fehler pro Komponente (SOLL)
- Anzahl Deployments im Testzeitraum (KANN)
- Betriebskosten / Komponente (KANN)
- Betriebskosten Gesamt (KANN)

Die angegebenen Kennzahlen sollten in die Kategorien MUSS, SOLL und KANN untergliedert werden, um eine Gewichtung für das Reporting zu ermöglichen. Welche Kriterien mit welcher Gewichtung betrachtet werden, ist in Übereinstimmung vor Beginn des Erprobungsbetriebs zwischen den beiden Partnern zu definieren. **Die angegebene Einstufung ist als Vorschlag zu verstehen.**

## Reporting

Der Erprobungsbetrieb wird durch ein engmaschiges Reporting begleitet, welches zwischen Erprobungspartner und dem ZenDiS abgestimmt ist. Das Reporting umfasst neben den in Kapitel [KPI](#kpi) aufgeführten Kennzahlen ein abgestimmtes Vorgehen bzgl. der Kommunikationswege und -prozesse zwischen Erprobungspartner und ZenDiS.

Folgende Fragestellungen sollten zwischen den Partnern verbindlich vor Beginn der Erprobung festgelegt sein:

- Meeting
  - Welche Meetings werden durchgeführt (feste Terminvorgaben)?
  - Welche Inhalte werden abgestimmt (feste Agenda)?
  - Welche Personen sind beteiligt? Bspw:
    - Operatives Management
    - Technik
    - Sicherheit
    - Datenschutz
- Kommunikationswege
  - Welche Kommunikationswege sind einzuhalten (bspw. bei IT-Sicherheitsvorfall)?

Die Herstellerkommunikation findet im Rahmen der Erprobung, abweichend zu anderen möglichen abgestimmten Kommunikationskanälen, ausschließlich über das ZenDiS statt. Dies dient der zentralen Steuerung und ggf. auch einer möglichen Erweiterung von openDesk über den reinen Erprobungsbetrieb hinweg.

Ferner beinhaltet das Reporting eine Form der Darstellung der Betriebsabläufe, der Sicherheit, des Datenschutzes und weiterer Anforderungen, die der IT-Betreiber an die Erprobung des openDesk verknüpft. Über den Umfang und die Form stimmt sich der Erprobungspartner mit dem ZenDiS ab.

## Überprüfung/Audit

Nach dem Erprobungsbetrieb findet eine Überprüfung der Erprobung statt. Als Grundlage für die Überprüfung dienen die KPI, die während der Erprobung erstellt wurden, in Kombination mit den Reports, auf die sich beide Parteien im Vorfeld verständigt haben.

Die Überprüfung kann auf einem oder mehreren der folgenden Möglichkeiten erfolgen:

- In Person / Remote zwischen den Parteien
- Durch einen Auditor oder zertifizierten Experten
- Durch das BMI selbst
- Durch eine unabhängige dritte Partei (externer Auditor)

Über den Umfang der Überprüfung sollten sich beide Parteien im Vorfeld verständigen. Bei der Überprüfung sollten jedoch mindestens die folgenden Punkte berücksichtigt werden:

- Betriebsabläufe
- Sicherheit beim IT-Betreiber
- Datenschutz beim IT-Betreiber

Eine Erweiterung der Überprüfung um ggf. gestellte weitere Anforderungen an den IT-Betreiber ist möglich. Die Anforderungen sollten bei der Betreiberauswahl vorliegen und für die Überprüfung qualifizierbar definiert sein.

## Übergang in den Betrieb

Nach erfolgreichem Audit und der damit verbundenen Feststellung des Erfolgs der Erprobung durch beide Seiten (Erprobungspartner und ZenDiS) erfolgt ggf. ein Übergang in den Produktivbetrieb.

Da im Rahmen der Erprobung ggf. nur ein Teil der Kunden des Erprobungspartners involviert waren, ist eine Planung zu erstellen, in der folgenden Fragestellungen geklärt werden:

- Ist eine sukzessive Erweiterung der Infrastruktur notwendig?
- Lässt sich die Erweiterung im laufenden Betrieb realisieren?
- Müssen ggf. bestehende Systeme portiert werden?

Zusätzlich sind in Bezug auf die eingesetzten Komponenten noch die folgenden Fragestellungen zu klären:

- Wie werden identifizierte Mängel beseitigt?
- Wie werden Zusatzanforderungen implementiert?

Sind alle infrastrukturellen und komponentenbezogenen Fragestellungen geklärt, erfolgt die Planung des Übergangs in den Produktivbetrieb durch den Erprobungspartner, der nun zu einem IT-Betreiber wird. Im Rahmen dieses Übergangs sind letztendlich noch Fragen bzgl. der involvierten Kunden im Rahmen der Erprobung zu klären und in die Planung aufzunehmen. Diese Fragestellungen sind:

- Müssen Kundendaten migriert werden?
- Bleiben die Zugänge (auch im Rahmen eines möglichen Infrastrukturumzugs) bestehen?
- Müssen ggf. Nachschulungen durchgeführt werden?
- Wie werden weitere Kunden in das System aufgenommen?

# Zusammenfassung

Die Zusammenfassung stellt eine Checkliste der wichtigsten Punkte des vorliegenden Dokumentes dar, die sich in die Planung, die Durchführung und die Überprüfung des PoC untergliedern. Für jeden Bereich wird eine beispielhafte Zusammenfassung der Punkte aus den vorangegangenen Kapiteln dargestellt. Sie dienen als Anhaltspunkt und können durch den Erprobungspartner und/oder das ZenDiS erweitert werden.

## Checkliste Erprobung

### Anforderungen an den IT-Betreiber vor Durchführung des PoC

| Thema                                                                                      | Erfüllt | Offen |
| ------------------------------------------------------------------------------------------ | ------- | ----- |
| IT-Dienstleister des öffentlichen Dienstes oder Privater Dienstleister mit Bundesbezug     |         |       |
| Rechenzentrumsbetrieb oder Gestaltungsmöglichkeiten des Rechenzentrumsbetriebs[^8] gegeben |         |       |
| &nbsp; &nbsp; Kubernetes (K8s) cluster                                                     |         |       |
| &nbsp; &nbsp; Domain und DNS                                                               |         |       |
| &nbsp; &nbsp; Ingress controller                                                           |         |       |
| &nbsp; &nbsp; Helm, Helmfile und HelmDiff                                                  |         |       |
| &nbsp; &nbsp; Volume provisioner                                                           |         |       |
| &nbsp; &nbsp; Zertifikatshandling mit cert-manager                                         |         |       |
| &nbsp; &nbsp; ISTIO service Mesh[^9]                                                       |         |       |
| &nbsp; &nbsp; CPU 8 Cores auf x64 oder x86 CPU (ARM ist aktuell nicht unterstützt)         |         |       |
| &nbsp; &nbsp; Mindestens 16 GB RAM, 32 GB empfohlen                                        |         |       |
| &nbsp; &nbsp; Größer 10 GB HDD oder SDD                                                    |         |       |
| Grundschutzfähigkeit, ISO27001 und/oder C5 gegeben                                         |         |       |
| Formalisierte Betriebsprozesse (bspw. ITSM) vorhanden                                      |         |       |
| Pro Kunde des Erprobungspartners mindestens 10 bis 15 Mitarbeiter                          |         |       |

*Anforderungskriterien an die Betreiberauswahl*

### Anforderungen an den Aufbau des Erprobungsbetriebs

| Thema                                                                       | Erfüllt | Offen |
| --------------------------------------------------------------------------- | ------- | ----- |
| Dokumentation liegt vor                                                     |         |       |
| &nbsp; &nbsp; Architekturkonzept                                            |         |       |
| &nbsp; &nbsp; Ggf. Betriebsdokumentation                                    |         |       |
| Vorbereitung Deployment                                                     |         |       |
| &nbsp; &nbsp; Vordefinierte Anpassungen der Komponenten durchgeführt        |         |       |
| &nbsp; &nbsp; Helm Charts angepasst                                         |         |       |
| Ausführung Deployment                                                       |         |       |
| &nbsp; &nbsp; Komponenten einwandfrei deployed (Kontrolle mittels HelmDiff) |         |       |
| &nbsp; &nbsp; Sicherheitsscan durchgeführt                                  |         |       |
| &nbsp; &nbsp; Backup/Rollback definiert                                     |         |       |
| CI/CD                                                                       |         |       |
| &nbsp; &nbsp; Deployment Automation eingerichtet                            |         |       |
| &nbsp; &nbsp; Feedbackloops für Deploymentfehler eingerichtet               |         |       |

*Anforderungen an den Aufbau des Erprobungsbetriebs*

### Anforderungen während der Erprobung

| Thema                                                                                   | Erfüllt | Offen |
| --------------------------------------------------------------------------------------- | ------- | ----- |
| Management                                                                              |         |       |
| &nbsp; &nbsp; Timeline der Erprobung angewandt                                          |         |       |
| &nbsp; &nbsp; Meilensteinplanung erstellt                                               |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Aufgaben- /Zieldefinition für Meilensteinplanung abgestimmt |         |       |
| &nbsp; &nbsp; Kommunikationswege definiert                                              |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Ansprechpartner namentlich bekannt                          |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Kontaktmöglichkeiten der Ansprechpartner festgelegt         |         |       |
| &nbsp; &nbsp; Berichte definiert                                                        |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Zwischenberichte festgelegt                                 |         |       |
| &nbsp; &nbsp; Ressourcenplanung bereitgestellt[^10]                                     |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; 56 Std./Woche Technik                                       |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; 16 Std./Woche Projektmanagement                             |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; 24 Std./Woche Administration / DevOps                       |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; 8 Std./Woche Teamleiter*innen / Ansprechpartner Management  |         |       |
| &nbsp; &nbsp; Personalverfügbarkeit geteilt                                             |         |       |
| &nbsp; &nbsp; Anwendung der KPI gem. diesem Dokument                                    |         |       |
| &nbsp; &nbsp; Zielkontrolle gem. Meilensteindefinition                                  |         |       |
| &nbsp; &nbsp; Termininhalte festgelegt                                                  |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Jour fixe                                                   |         |       |
| Produkt                                                                                 |         |       |
| &nbsp; &nbsp; Anforderungen an die betriebliche Bereitstellung definiert                |         |       |
| &nbsp; &nbsp; Patch-Management-Prozess eingeführt                                       |         |       |
| &nbsp; &nbsp; Technischer Releaseprozess etabliert                                      |         |       |
| Operativer Prozess                                                                      |         |       |
| &nbsp; &nbsp; ITSM etabliert                                                            |         |       |
| &nbsp; &nbsp; Softwareüberwachung eingerichtet                                          |         |       |
| &nbsp; &nbsp; Supportprozess definiert                                                  |         |       |
| &nbsp; &nbsp; KPI Protokollierung etabliert                                             |         |       |
| &nbsp; &nbsp; Monitoring Tools eingerichtet                                             |         |       |

*Anforderungen während der Erprobung*

### Anforderungen zur Evaluierung des PoC

| Thema                                                                                                          | Erfüllt | Offen |
| -------------------------------------------------------------------------------------------------------------- | ------- | ----- |
| Dokumentation                                                                                                  |         |       |
| &nbsp; &nbsp; Liefergegenstände vorhanden                                                                      |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Zwischenberichte gem. Meilensteindefinition vorliegend                             |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Zwischenberichte beidseitig abgenommen                                             |         |       |
| &nbsp; &nbsp; KPI zur Auswertung vorliegend                                                                    |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Auswertung IT-Monitoring als Verlaufsprotokoll                                     |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Evaluierung Erprobungsbetrieb gem. Kapitel [Erprobungsbetrieb](#erprobungsbetrieb) |         |       |
| Produkt                                                                                                        |         |       |
| &nbsp; &nbsp; Änderungswünsche zusammengetragen                                                                |         |       |
| &nbsp; &nbsp; Mängelliste erstellt                                                                             |         |       |
| &nbsp; &nbsp; Performance KPI vorhanden und vollständig                                                        |         |       |
| Überführung in produktiven Betrieb                                                                             |         |       |
| &nbsp; &nbsp; Audit durchgeführt                                                                               |         |       |
| &nbsp; &nbsp; Zeitplanung erstellt                                                                             |         |       |
| &nbsp; &nbsp; Konzept zur Beseitigung der Mängel gem. Mängelliste vorliegend                                   |         |       |
| &nbsp; &nbsp; Planung zur Umsetzung der Änderungswünsche                                                       |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Dokumentation der Änderungswünsche                                                 |         |       |
| &nbsp; &nbsp; &nbsp; &nbsp; Zeitplanung erstellt                                                               |         |       |

*Anforderungen an die Durchführung des PoC*

# Fußnotenverzeichnis

[^1]: ISO/IEC27001:2022, veröffentlicht Okt. 2022 auf https://www.iso.org/standard/27001
[^2]: Cloud Computing Compliance Criteria Catalogue, kann unter https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Empfehlungen-nach-Angriffszielen/Cloud-Computing/Kriterienkatalog-C5/C5_AktuelleVersion/C5_AktuelleVersion_node.html in der Version von Okt. 2020 bezogen werden
[^3]: Ist nicht zwingend zur Überprüfung notwendig; Muss jedoch spätestens bei Feststellung des Erfolgs im Rahmen des Übergangs in den Produktivbetrieb (vgl. hierzu Kapitel [Übergang in den Betrieb](#übergang-in-den-betrieb)) erfolgen
[^4]: Responsible = Verantwortlich für die Umsetzung bzw. Durchführung, <br> Accountable = Verantwortlich für die Prüfung, Genehmigung und Freigabe <br> Consulted = Begleitet fachlich beratend <br> Informed = Werden über den Fortschritt informiert
[^5]: Erprobung in Kooperation mit der BWI GmbH in der zweiten Jahreshälfte 2023
[^6]: Vgl. openDesk unter https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk
[^7]: (splunk>, 2023), redaktionell bearbeitet
[^8]: Vgl. https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/blob/main/docs/requirements.md, Stand 16.01.2024, aufgerufen am 26.01.2024, Die Anforderungen stellen Minimalanforderungen dar und sind je nach gewählten Betreibermodell (Anzahl Kunden des Erprobungspartner) entsprechend zu skalieren.
[^9]: Vgl. https://istio.io, Stand 2024, aufgerufen am 05.02.2024
[^10]: Die angegebenen Werte (Std./Woche) sollten mindestens erfüllt sein. Aufgaben können dabei auch in Personalunion stattfinden.
